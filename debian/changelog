gnustep-examples (1:1.4.0-3) unstable; urgency=medium

  * Bump standards version to 4.5.0.
  * Update Vcs fields.
  * debian/menu: dropped.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 13 Feb 2020 13:36:22 +0100

gnustep-examples (1:1.4.0-2) unstable; urgency=medium

  * Update my name.
  * Bump debhelper version to 11.
  * Bump standards version to 4.1.3.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 09 Feb 2018 20:09:15 +0100

gnustep-examples (1:1.4.0-1) unstable; urgency=medium

  * New upstream release:
    - Compatible with gnustep-gui/0.24 (Closes: #749747).
  * debian/watch: Update to look at usr-apps as well.  Use
    pgpsigurlmangle.
  * debian/compat: Set to 9.
  * debian/control (Build-Depends): Require libgnustep-gui-dev (>= 0.24)
    and debhelper (>= 9).  Drop dpkg-dev.
    (Vcs-Git): Use the canonical URI.
    (Standards-Version): Compliant with 3.9.6 as of this release.
  * debian/patches/typo-fix.patch: New.
  * debian/patches/series: Update.
  * debian/preinst:
  * debian/README.source: Delete.
  * debian/rules: Rewrite for modern dh.
  * debian/upstream/signing-key.pgp:
  * debian/source/include-binaries:
  * debian/install: New file.
  * debian/Calculator.desktop:
  * debian/Ink.Desktop: Add Keywords field.
  * debian/copyright: Switch to format 1.0, add more copyright
    holders/licenses.

 -- Yavor Doganov <yavor@gnu.org>  Tue, 21 Oct 2014 14:03:35 +0300

gnustep-examples (1:1.3.0-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/no-MyGL.patch: Remove; already present upstream.
  * debian/patches/series: Update.
  * debian/control (Standards-Version): Set to 3.9.3; no changes needed.
    (Build-Depends): Remove quilt.  Add dpkg-dev (>= 1.16.1~), for
    hardening support.
    (Vcs-Arch, Vcs-Git, Vcs-Browser): Switch from Arch to Git.
  * debian/copyright: Rewrite to reflect reality; thanks Axel Beckert.
  * debian/source/format: Switch to 3.0 (quilt).
  * debian/rules: Don't include /usr/share/quilt/quilt.make.  Eliminate
    patch/unpatch dependencies.  Enable hardening.  Remove GS_USE_FHS
    conditional, always true.

 -- Yavor Doganov <yavor@gnu.org>  Fri, 01 Jun 2012 20:58:01 +0300

gnustep-examples (1:1.2.0-3) unstable; urgency=low

  * debian/compat: Set to 7.
  * debian/control (Section): Change to `gnustep'.
    (Uploaders): Remove Eric and Hubert, add myself.
    (Build-Depends): Bump debhelper to >= 7, unversion
    libgnustep-gui-dev.  Add quilt and imagemagick.  Remove
    libgl1-mesa-dev and libglu1-mesa-dev.
    (Description): Extend.
    (Standards-Version): Compliant with 3.8.4 as of this release.
    (Homepage, Vcs-Arch): New fields.
  * debian/patches/sensible-browser.patch: New; move from the diff.gz.
  * Restore apps list supported/enabled by default upstream (remove
    gui/GNUmakefile local modification from diff.gz); incidentally
    fixes FTBFS with gnustep-gui/0.18.0 (Closes: #582079).
  * debian/patches/no-MyGL.patch: New; disable MyGL as well following
    upstream.
  * debian/patches/series: New file.
  * debian/rules: Adapt for quilt.  Get rid of gs_make.  Cleanups.
    (pkg, dir): Replace with...
    (d_app): ...which is more common; all uses updated.
    (OPTFLAG): No longer define; rework noopt handling to be compatible
    with gnustep-make/2.4.x (Closes: #582080).
    (LDFLAGS): New variable.
    (configure, configure-stamp): Remove; useless.
    (build-stamp): Generate XPM icons for the menu file.
    (clean-patched): Delete them.  Simplify rule.
    (install): Replace dh_clean -k with dh_prep.  Do not invoke
    dh_installdirs.  Do not install lintian overrides.
    (binary-arch): Don't install upstream README files; obsolete.  Remove
    unneeded dh_* commands.  Install the newly added generic manpage and
    create the corresponding symlinks.  Install the XPM icons and .desktop
    files.  Conditionally move Resources to /usr/share/GNUstep.
  * debian/overrides:
  * debian/menu.test:
  * debian/README.Debian: Delete.
  * debian/source/format:
  * debian/README.source:
  * debian/gnustep-examples.1:
  * debian/Calculator.desktop:
  * debian/Ink.desktop:
  * debian/preinst: New file.
  * debian/watch: Bump version, prepend opts=pasv, don't uupdate.
  * debian/menu: Retain only the most useful apps, i.e. Ink and
    Calculator.  Fix command, section, longtitle fields.  Add icon.
  * debian/copyright: Clarify copyright vs. license.

 -- Yavor Doganov <yavor@gnu.org>  Thu, 27 May 2010 00:47:50 +0300

gnustep-examples (1:1.2.0-2) unstable; urgency=low

  * debian/control:
    * Update Gürkan's email address.
    * Bump standards version to 3.7.3. (no other changes needed)
    * Add dependency on ${gnustep:Depends}.

 -- Hubert Chathi <uhoreg@debian.org>  Tue, 17 Jun 2008 07:14:14 -0400

gnustep-examples (1:1.2.0-1.1) unstable; urgency=high

  * Non-maintainer upload.
  * Optimised (-O2) compiling fixes FTBFS (Closes: #457555).

 -- Luk Claes <luk@debian.org>  Tue, 01 Jan 2008 02:05:17 +0100

gnustep-examples (1:1.2.0-1) unstable; urgency=low

  * New upstream release.
  * Build-depend on libgl1-mesa-dev and libglu1-mesa-dev for GL example.
  * Change default web browser for example service to sensible-browser.
  * Update maintainer address.
  * debian/menu: s/Apps/Applications/g, and other menu updates.

 -- Hubert Chathi <uhoreg@debian.org>  Fri,  5 Oct 2007 19:49:55 -0400

gnustep-examples (1:1.1.0-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.7.2. (no changes)
  * Rebuild against latest GNUstep libraries.
  * Don't build GPuzzle (removed by upstream).

 -- Hubert Chan <hubert@uhoreg.ca>  Mon, 25 Sep 2006 18:27:25 -0400

gnustep-examples (1:1.0.0-2) unstable; urgency=low

  * Rebuild against latest GNUstep libraries.
  * Bump standards version to 3.6.2.1.

 -- Hubert Chan <hubert@uhoreg.ca>  Fri, 30 Dec 2005 16:37:05 -0700

gnustep-examples (1:1.0.0-1) unstable; urgency=low

  * New upstream version.
  * Don't suffix application names with ".app".
  * Add ToolbarExample.
  * Add a debian/watch file.

 -- Eric Heintzmann <eric@gnustep.fr.st>  Sat, 27 Nov 2004 17:50:04 +0100

gnustep-examples (1:0.9.7-3) unstable; urgency=medium

  * debian/control:
    - Rebuild using latest GNUstep Core packages (closes #259536).
    - Update Debian GNustep maintainers e-mail address.
    - Bump Standards-Version to 3.6.1.1.
  * debian/menu:
    - Sufix application names with ".app".
    - No longer source GNUstep.sh before openapp.
  * debian/README.Debian:
    - Remove instructions about sourcing GNUstep.sh.
    - Add GFractal and GPuzzle in the application list.
  * debian/rules:
    - Add messages=yes flag when building.

 -- Eric Heintzmann <eric@gnustep.fr.st>  Sun, 18 Jul 2004 16:00:53 +0200

gnustep-examples (1:0.9.7-2) unstable; urgency=low

  * Update debian/rules to use flattened directory structure :
     remove GS_HOST, GS_CPU, GS_OS, GS_LIB_DIR, GS_COMBO_DIR variables.
  * Update Build-Depends field in debian/control file.
  * Update Standard-Version to 3.6.1.
  * Update to Debhelper compatibility level 4.
  * Update debian/README.Debian file.
  * Update debian/copyright file.
  * New Co-Maintainer.
  * New debian/menu file.
  
 -- Eric Heintzmann <eric@gnustep.fr.st>  Thu,  7 Aug 2003 01:44:51 +0200

gnustep-examples (1:0.9.7-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Sat, 20 Jul 2002 18:29:24 +0200

gnustep-examples (0.9.020407-1) unstable; urgency=low

  * CVS snapshot of the GNUstep examples.

 -- Matthias Klose <doko@debian.org>  Sun,  7 Apr 2002 15:21:59 +0200

gnustep-examples (0.9.2.90-1) unstable; urgency=low

  * Initial Release.

 -- Matthias Klose <doko@debian.org>  Sat, 19 Jan 2002 14:58:19 +0100
